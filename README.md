# Less1.4
Необходимо собрать nginx из исходников со стандартными модулями и запустить бинарник во втором образе.<br/>
https://habr.com/ru/post/321468/<br/>
Образы Debian9<br/>
При запуске примаунтить конфиг в контейнер.<br/>
https://www.thegeekstuff.com/2011/07/install-nginx-from-source/<br/>
https://docs.docker.com/develop/develop-images/multistage-build/<br/>
https://docs.docker.com/storage/bind-mounts/<br/>
<br/>
<br/>
Задача для прохождения на 100% (выполнять не обязательно)<br/>
Добавить в сборку<br/>
https://github.com/openresty/lua-nginx-module<br/>

## Собираем nginx из исходников в общем:
Школа, интернет и сопромат подсказывают нам, что для сборки чего либо из исходных кодов неплохо было бы заиметь в<br/>
нашем докер контейнере компилятор Си, нужна будет еще утилита make, для выкачивания чего-либо по ссылке отлично пойдет wget,<br/> еще нужен будет гит, библиотека для регулярных выражений libpcre3 и libpcre3-dev, а также библиотеки для компресии и ssl<br/>
zlib1g, zlib1g-dev, libssl-dev. Все это будет инсталлить и опишем в докерфайле. Но начать докерфайл стоит все таки с<br/> аргументов, в них поместим версии того чего нам надо скачать, так как иногда возникает задача обновиться на какую-либо<br/> другую версию, то поменять ее можно будет в одном месте в самом начале, а не искать глазами в коде, ну и пути к папкам<br/> конечно же тоже можно засетать аргументах. Ну и ARG можно юзать с командой docker build, что не маловажно!<br/>
Итого значит наш мультистейдж докерфайл будет выглядеть вот так:<br/>
```bash
# Аргументы для версии nginxm, luajit и путей.
ARG nginx_version=1.23.3
ARG luajit_version=2.0.5
ARG LUAJIT_LIB=/usr/local/lib
ARG LUAJIT_INC=/usr/local/include/luajit-2.0


# Здесь получатеся билдим nginx из исходников, предварительно выкачав все что нам нужно.
FROM debian:9 as build
WORKDIR /opt
ARG nginx_version
ARG luajit_version
ARG LUAJIT_LIB
ARG LUAJIT_INC
RUN apt update && \
    apt install -y gcc make wget libpcre3-dev zlib1g-dev git && \
    wget http://nginx.org/download/nginx-${nginx_version}.tar.gz && \
    tar -xzvf nginx-${nginx_version}.tar.gz && \
    git clone https://github.com/openresty/lua-nginx-module && \
    wget http://luajit.org/download/LuaJIT-${luajit_version}.tar.gz && \
    tar -xzvf LuaJIT-${luajit_version}.tar.gz && \
    cd LuaJIT-${luajit_version} && make && make install && \
    cd ../nginx-${nginx_version} && \
    ./configure --add-module=../lua-nginx-module --with-ld-opt="-Wl,-rpath,/opt/LuaJIT-${luajit_version}" && \
    make && \
    make install


# А здесь скопируем получившиеся бинарники и стартанем nginx.
FROM debian:9
ARG luajit_version
ARG LUAJIT_LIB
COPY --from=build /usr/local/nginx/ /usr/local/nginx
COPY --from=build /usr/local/lib/libluajit-5.1.a /usr/local/lib/
COPY --from=build /usr/local/lib/libluajit-5.1.so /usr/local/lib/
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/
COPY --from=build /usr/local/lib/libluajit-5.1.so.${luajit_version} /usr/local/lib/
ENV LD_LIBRARY_PATH=${LUAJIT_LIB}
WORKDIR /usr/local/nginx/sbin
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
```

